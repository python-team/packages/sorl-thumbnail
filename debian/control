Source: sorl-thumbnail
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Martin <debacle@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               graphicsmagick,
               imagemagick,
               locales,
               lsof,
               python3-all,
               python3-django,
               python3-pil,
               python3-pgmagick,
               python3-pytest,
               python3-pytest-django,
               python3-setuptools,
               python3-setuptools-scm,
               python3-sphinx (>= 1.0.7+dfsg)
Build-Conflicts: locales-all (<< 2.21-1)
Standards-Version: 4.2.0
Rules-Requires-Root: no
Homepage: https://github.com/jazzband/sorl-thumbnail
Vcs-Git: https://salsa.debian.org/python-team/packages/sorl-thumbnail.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/sorl-thumbnail

Package: python3-sorl-thumbnail
Architecture: all
Depends: python3-django,
         python3-pil | python3-pgmagick | imagemagick | graphicsmagick,
         ${misc:Depends},
         ${python3:Depends}
Suggests: python3-redis
Description: thumbnail support for the Django framework (Python3 version)
 sorl-thumbnail provides a convenient way to create and manage image thumbnails
 in a Django project. It offers integration into several parts of Django like
 the admin system by showing thumbnails for ImageFields or through a ImageField
 that also takes care of deleting thumbnail files when the referencing object
 is removed.
 .
 Other features include:
  * Django storage support
  * Pluggable Engine support (PIL, pgmagick, ImageMagick, GraphicsMagick,
    or Wand)
  * Pluggable Key Value Store support (Django cache, redis)
  * Pluggable Backend support (i.e. different thumbnail filename schema)
  * Dummy generation (placeholders)
  * Flexible, simple syntax, generates no html
  * CSS style cropping options
  * Margin calculation for vertical positioning
 .
 This package contains the Python 3 version of the library.

Package: python-sorl-thumbnail-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: thumbnail support for the Django framework (Documentation)
 sorl-thumbnail provides a convenient way to create and manage image thumbnails
 in a Django project. It offers integration into several parts of Django like
 the admin system by showing thumbnails for ImageFields or through a ImageField
 that also takes care of deleting thumbnail files when the referencing object
 is removed.
 .
 Other features include:
  * Django storage support
  * Pluggable Engine support (PIL, pgmagick, ImageMagick, GraphicsMagick,
    or Wand)
  * Pluggable Key Value Store support (Django cache, redis)
  * Pluggable Backend support (i.e. different thumbnail filename schema)
  * Dummy generation (placeholders)
  * Flexible, simple syntax, generates no html
  * CSS style cropping options
  * Margin calculation for vertical positioning
 .
 This package contains the documentation.
